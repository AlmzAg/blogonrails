class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :name, null: false
      t.boolean :role, null: false, default: false
      t.datetime :reg_date
      t.timestamps
    end
  end
  
  def down
     drop_table :users
  end

end
